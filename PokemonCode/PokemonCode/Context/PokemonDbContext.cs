﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PokemonCode.Models.Request;
using PokemonCode.Models.Request.Pokemon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Context
{
    public class PokemonDbContext : DbContext
    {
        public PokemonDbContext(DbContextOptions<PokemonDbContext> options) : base(options)
        {
        }
        public DbSet<Pokemon> Pokemons { set; get; }
        public virtual DbSet<Pokemon_Types> pokemon_Types { set; get; }
        public DbSet<Models.Request.Type.Type> Types { set; get; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Pokemon_Types>().HasKey(br => new { br.PokemonId, br.TypeId });
            modelBuilder.Entity<Pokemon>().HasData(
            new Pokemon() { PokemonID = 1, PokemonCode = "00001", PokemonName = "Bulbasaur ", Height = 2.04M, Weight = 6.8M, Gender = 0},
            new Pokemon() { PokemonID = 2, PokemonCode = "00004", PokemonName = "Charmander  ", Height = 2M, Weight = 8.5M, Gender = 0 });

            modelBuilder.Entity<Models.Request.Type.Type>().HasData(
            new Models.Request.Type.Type() { TypeID = 1, TypeName = "Fire" },
            new Models.Request.Type.Type() { TypeID = 2, TypeName = "Grass" },
            new Models.Request.Type.Type() { TypeID = 3, TypeName = "Poison" });

            modelBuilder.Entity<Pokemon_Types>().HasData(
            new Pokemon_Types() { PokemonId = 1, TypeId = 2 },
            new Pokemon_Types() { PokemonId = 1, TypeId = 3 },
            new Pokemon_Types() { PokemonId = 2, TypeId = 1 });
        }
    }
}
