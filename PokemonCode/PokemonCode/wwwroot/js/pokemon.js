﻿var pokemon = pokemon || {}

pokemon.drawDataTable = function () {
    dataTableOption = $("#tbPokemon").DataTable({
        "processing": true, // for show progress bar  
        "serverSide": true, // for process server side  
        "filter": true, // this is for disable filter (search box)  
        "orderMulti": false, // for disable multiple column at once  
        "ajax": {
            "url": "/Pokemon/Gets",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "pokemonCode",
                "name": "PokemonCode",
                "autoWidth": true,
                "title": "Mã Pokemon",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "pokemonName",
                "name": "pokemonName",
                "autoWidth": true,
                "title": "Tên Pokemon",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "typeNames",
                "name": "TypeName",
                "autoWidth": true,
                "title": "Hệ",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "pokemonID",
                "name": "PokemonID",
                render: function (data, type, full, meta) {
                    var editBtn = "<a href='javascript:;' onclick=pokemon.showEditModal('" + data + "')><i class='fas fa-edit'></i></a>";
                    var deleteBtn = "<a href='javascript:;' onclick=pokemon.delete('" + data + "')><i class='fas fa-trash-alt'></i></a>";
                    return $("<div>").append(editBtn + " " + deleteBtn).html();
                },
                "title": "Thao tác",
            }

        ]
    });
};

pokemon.showModal = function () {
    $('#addEditPokemon').modal('show');
};

pokemon.save = function () {
    var pokemonObj = {};
    pokemonObj["PokemonID"] = $('#PokemonID').val();
    pokemonObj["PokemonCode"] = $('#PokemonCode').val();
    pokemonObj["PokemonName"] = $('#PokemonName').val();
    pokemonObj["Height"] = $('#Height').val();
    pokemonObj["Weight"] = $('#Weight').val();
    pokemonObj["Gender"] = $('#Gender').val();
    pokemonObj["TypeIDs"] = $('#TypeID').val();
    $.ajax({
        url: '/Pokemon/Save',
        method: 'POST',
        dataType: 'json',
        data: JSON.stringify(pokemonObj), //convert obj to json
        contentType: 'application/json',
        success: function (data) {
            if (data.status === 1) {
                alert(data.message);
                pokemon.resetForm();
                pokemon.reloadDataTable();
                $('#addEditPokemon').modal('hide');
            }
        }

    });
};


pokemon.delete = function (id) {
    var result = confirm("Want to delete?");
    if (result) {
        $.ajax({
            url: '/Pokemon/Delete/' + id,
            method: 'DELETE',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.status === 1) {
                    alert(data.message)
                    pokemon.reloadDataTable();


                }
            }

        });
    }

};

pokemon.resetForm = function () {
    $('#PokemonID').val(0);
    $('#PokemonCode').val('');
    $('#PokemonName').val('');
    $('#TypeID').val('');
    $('#Height').val('');
    $('#Weight').val('');
    $('#Gender').val('');
    $('#addEditPokemon').find('.modal-title').text("Add Pokemon");
};

pokemon.init = function () {
    //pokemon.GetTypes();
    pokemon.resetForm();
    $('.select').select2();
    pokemon.drawDataTable();
};

pokemon.showEditModal = function (id) {
    $.ajax({
        url: '/pokemon/Get/' + id,
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $('#PokemonID').val(response.pokemonID);
                $('#PokemonCode').val(response.pokemonCode);
                $('#PokemonName').val(response.pokemonName);
                $('#TypeID').val(response.typeIDs);
                $('#Height').val(response.height);
                $('#Weight').val(response.weight);
                $('#Gender').val(response.gender);

                pokemon.GetTypes(response.typeIDs);
                
                //if (response.typeIDs != null) {
                //    _.map(response.typeIDs, function (type) {
                //        $('#TypeID').select2().append('<option value="' + type.Id + '" selected></option>');
                //    });
                //}

                $('#addEditPokemon').find('.modal-title').text("Edit Pokemon");
                $('#addEditPokemon').modal('show');
            }
        }

    });
};
pokemon.GetTypes = function (typeIDs) {
    $.ajax({
        url: '/pokemon/GetTypes',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $('#TypeID').empty();
                $.each(response, function (index, value) {
                    var row = "<option value = '" + value.typeID + "'" + (typeIDs.includes(value.typeID) ? 'selected' : '') + ">" + value.typeName + "</option>";
                    $('#TypeID').append(row);
                });
            }
        }
    });
};

//pokemon.GetTypeIdPoke = function (id) {
//    $.ajax({S
//        url: '/pokemon/GetTypeIdPoke/' + id,
//        method: 'GET',
//        dataType: 'json',
//        contentType: 'application/json',
//        success: function (data) {
//            if (data.code === 1) {
//                var response = data.response;
//                $('#TypeID').empty();
//                $.each(response, function (index, value) {
//                    $('#TypeID').append("<option value = " + value.typeID + ">" + value.typeName + "</option>");
//                });
//            }
//        }
//    });
//};


pokemon.reloadDataTable = function () {
    dataTableOption.ajax.reload(null, false);
};

$(document).ready(function () {
    pokemon.init();
});