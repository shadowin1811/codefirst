﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Models.Reponse.Pokemon
{
    public class PokemonDetail
    {
        [Key]
        public int PokemonID { get; set; }

        public string PokemonCode { get; set; }

        public string PokemonName { get; set; }
        public int TypeID { get; set; }
        //public List<string> TypeName { get; set; }
        //public string TypeNames => TypeName != null ? string.Join(", ", TypeName) : string.Empty;
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public int? Gender { get; set; }
        //    public string Avatar { get; set; }
        //    public bool IsDelete { get; set; } = false;
    }
}
