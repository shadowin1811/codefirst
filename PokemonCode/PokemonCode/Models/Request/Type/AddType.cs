﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Models.Request.Type
{
    public class AddType
    {
        public int TypeId { get; set; }
        public string Typename { get; set; }
    }
}
