﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Models.Request.Pokemon_Type
{
    public class PokemonType
    {
        public int PokemonId { get; set; }

        public int TypeId { get; set; }

    }
}
