﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Models.Request.Pokemon
{
    public class PokemonEdit
    {
        public int PokemonID { get; set; }
        public string PokemonCode { get; set; }
        public string PokemonName { get; set; }
        public List<int> TypeIDs { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public int? Gender { get; set; }
        public string Avatar { get; set; }
        public bool IsDelete { get; set; }
    }
}
