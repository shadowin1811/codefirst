﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Models.Request.Pokemon
{
    public class Pokemon
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PokemonID { get; set; }
        [Required]
        [StringLength(50)]
        public string PokemonCode { get; set; }
        [Required]
        [StringLength(50)]
        public string PokemonName { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }
        public int? Gender { get; set; }
        public string Avatar { get; set; }
        public bool IsDelete { get; set; } = false;
    }
}
