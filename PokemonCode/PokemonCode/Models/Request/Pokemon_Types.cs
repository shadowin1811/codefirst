﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonCode.Models.Request
{
    public class Pokemon_Types
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Pokemon")]
        public int PokemonId { get; set; }
        public PokemonCode.Models.Request.Pokemon.Pokemon Pokemon { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Type")]
        public int TypeId { get; set; }
        public PokemonCode.Models.Request.Type.Type Type { get; set; }

    }
}
