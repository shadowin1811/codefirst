﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using PokemonCode.Context;
using PokemonCode.Models.Reponse.Pokemon;
using PokemonCode.Models.Request.Pokemon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Linq;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using PokemonCode.Models.Request.Type;
using PokemonCode.Models.Request;

namespace PokemonCode.Controllers
{
    public class PokemonController : Controller
    {
        private readonly PokemonDbContext _pokemonDbContext;
        public PokemonController(PokemonDbContext pokemonDbContext)
        {
            _pokemonDbContext = pokemonDbContext;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Save([FromBody] PokemonCreate model)
        {
            var result = _pokemonDbContext.pokemon_Types.FirstOrDefault(pt => pt.PokemonId == model.PokemonID);
            if (result == null)
            {
                if (model.PokemonID == 0)
                {
                    try
                    {
                        var addPokemon = new Pokemon()
                        {
                            PokemonID = model.PokemonID,
                            PokemonCode = model.PokemonCode,
                            PokemonName = model.PokemonName,
                            Gender = model.Gender,
                            Height = model.Height,
                            Weight = model.Weight,
                        };
                        _pokemonDbContext.Pokemons.Add(addPokemon);
                        _pokemonDbContext.SaveChanges();

                        var maxId = _pokemonDbContext.Pokemons.OrderByDescending(x => x.PokemonID).FirstOrDefault().PokemonID;

                        foreach (var item in model.TypeIDs)
                        {
                            var _types = new Pokemon_Types()
                            {
                                PokemonId = maxId,
                                TypeId = item
                            };
                            _pokemonDbContext.pokemon_Types.Add(_types);
                            _pokemonDbContext.SaveChanges();
                        }

                        if (_pokemonDbContext != null)
                        {
                            return Json(new { status = 1, message = model.PokemonName + "has been created successfully." });
                        }
                        return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    try
                    {
                        var editPokemon = new Pokemon()
                        {
                            PokemonID = model.PokemonID,
                            PokemonCode = model.PokemonCode,
                            PokemonName = model.PokemonName,
                            Gender = model.Gender,
                            Height = model.Height,
                            Weight = model.Weight,
                        };
                        _pokemonDbContext.Pokemons.Update(editPokemon);
                        _pokemonDbContext.SaveChanges();

                        var pokemonTypes = _pokemonDbContext.pokemon_Types.Where(pt => pt.PokemonId == model.PokemonID).ToList();
                        foreach (var item in pokemonTypes)
                        {
                            _pokemonDbContext.pokemon_Types.Remove(item);
                            _pokemonDbContext.SaveChanges();
                        }

                        foreach (var item in model.TypeIDs)
                        {
                            var _types = new Pokemon_Types()
                            {
                                PokemonId = model.PokemonID,
                                TypeId = item
                            };
                            _pokemonDbContext.pokemon_Types.Add(_types);
                            _pokemonDbContext.SaveChanges();
                        }

                        if (_pokemonDbContext != null)
                        {
                            return Json(new { status = 1, message = model.PokemonName + "has been update successfully." });
                        }
                        return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                try
                {
                    var editPokemon = new Pokemon()
                    {
                        PokemonID = model.PokemonID,
                        PokemonCode = model.PokemonCode,
                        PokemonName = model.PokemonName,
                        Gender = model.Gender,
                        Height = model.Height,
                        Weight = model.Weight,
                    };
                    _pokemonDbContext.Pokemons.Update(editPokemon);
                    _pokemonDbContext.SaveChanges();

                    var pokemonTypes = _pokemonDbContext.pokemon_Types.Where(pt => pt.PokemonId == model.PokemonID).ToList();
                    foreach (var item in pokemonTypes)
                    {
                        _pokemonDbContext.pokemon_Types.Remove(item);
                        _pokemonDbContext.SaveChanges();
                    }

                    foreach (var item in model.TypeIDs)
                    {
                        var _types = new Pokemon_Types()
                        {
                            PokemonId = model.PokemonID,
                            TypeId = item
                        };
                        _pokemonDbContext.pokemon_Types.Add(_types);
                        _pokemonDbContext.SaveChanges();
                    }

                    if (_pokemonDbContext != null)
                    {
                        return Json(new { status = 1, message = model.PokemonName + "has been update successfully." });
                    }
                    return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            }

            //public JsonResult Get(int id)
            //{
            //    var employeeEdit = new PokemonEdit();
            //    {

            //    }
            //    return Json(new { response = employeeEdit, code = 1 });
            //}

            //#region Add
            //[HttpGet]
            //public IActionResult AddPokemon()
            //{
            //    return View();
            //}

            //[HttpPost]
            //public IActionResult AddPokemon(PokemonCreate create)
            //{

            //    try
            //    {
            //        var model = new Pokemon()
            //        {
            //            PokemonID = create.PokemonID,
            //            PokemonCode = create.PokemonCode,
            //            PokemonName = create.PokemonName,
            //            Gender = create.Gender,
            //            Height = create.Height,
            //            Weight = create.Weight,
            //        };

            //        if (_pokemonDbContext != null)
            //        {
            //            _pokemonDbContext.Pokemons.Add(model);
            //            _pokemonDbContext.SaveChanges();
            //            TempData["Message"] = model.PokemonName + "added successfully.";

            //        }
            //        else
            //        {
            //            TempData["Errors"] = "Something went wrong, please contact administrator.";
            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }

            //    return RedirectToAction("Index");
            //}
            //#endregion

            #region View, Sort, Search pokemon
            public ActionResult Gets()
            {
                try
                {
                    var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                    // Skiping number of Rows count  
                    var start = Request.Form["start"].FirstOrDefault();
                    // Paging Length 10,20  
                    var length = Request.Form["length"].FirstOrDefault();
                    // Sort Column Name  
                    var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                    // Sort Column Direction ( asc ,desc)  
                    var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                    // Search Value from (Search box)  
                    var searchValue = Request.Form["search[value]"].FirstOrDefault();

                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    var _pokemons = (from p in _pokemonDbContext.Pokemons
                                         //join pt in _pokemonDbContext.pokemon_Types on p.PokemonID equals pt.PokemonId
                                     where p.IsDelete == false
                                     select new PokemonView()
                                     {
                                         PokemonID = p.PokemonID,
                                         PokemonCode = p.PokemonCode,
                                         PokemonName = p.PokemonName,
                                         TypeName = (from pk in _pokemonDbContext.Pokemons
                                                     join pkt in _pokemonDbContext.pokemon_Types on pk.PokemonID equals pkt.PokemonId
                                                     join type in _pokemonDbContext.Types on pkt.TypeId equals type.TypeID
                                                     where pk.PokemonID == p.PokemonID
                                                     select type.TypeName).ToList()
                                     }).ToList();

                    var distinctItems = _pokemons.Distinct(new DistinctItemComparer());


                    //Sorting  
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                    {
                        var prop = GetProperty(sortColumn);
                        if (sortColumnDirection == "asc")
                        {
                            distinctItems = distinctItems.OrderBy(prop.GetValue).ToList();
                        }
                        else
                        {
                            distinctItems = distinctItems.OrderByDescending(prop.GetValue).ToList();
                        }
                    }
                    //Search  
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        _pokemons = (from p in _pokemons
                                     join pt in _pokemonDbContext.pokemon_Types on p.PokemonID equals pt.PokemonId
                                     join t in _pokemonDbContext.Types on pt.TypeId equals t.TypeID
                                     where p.PokemonName.Contains(searchValue)
                                     select p).ToList();
                        distinctItems = _pokemons.Distinct(new DistinctItemComparer());
                    }

                    //total number of rows count   
                    recordsTotal = distinctItems.Count();
                    //Paging   
                    var data = distinctItems.Skip(skip).Take(pageSize).ToList();
                    //Returning Json Data  
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

                }
                catch (Exception)
                {
                    throw;
                }
            }
            #endregion
            public ActionResult Detail(int id)
            {
                try
                {
                    var _pokemondetails = (from p in _pokemonDbContext.Pokemons
                                           join pt in _pokemonDbContext.pokemon_Types on p.PokemonID equals pt.PokemonId
                                           join tp in _pokemonDbContext.Types on pt.TypeId equals tp.TypeID
                                           where p.PokemonID == id
                                           select new PokemonDetail
                                           {
                                               PokemonID = p.PokemonID,
                                               PokemonCode = p.PokemonCode,
                                               PokemonName = p.PokemonName,
                                               Height = p.Height,
                                               Weight = p.Weight,
                                               Gender = p.Gender,
                                               TypeID = tp.TypeID
                                               //TypeName = (from pk in _pokemonDbContext.Pokemons
                                               //            join pkt in _pokemonDbContext.pokemon_Types on pk.PokemonID equals pkt.PokemonId
                                               //            join type in _pokemonDbContext.Types on pkt.TypeId equals type.TypeID
                                               //            where pk.PokemonID == p.PokemonID
                                               //            select type.TypeName).ToList()
                                           }).FirstOrDefault();


                    return Json(new { response = _pokemondetails, code = 1 });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public JsonResult Get(int id)
            {
                try
                {
                    var _pokemongets = (from p in _pokemonDbContext.Pokemons
                                            //join pt in _pokemonDbContext.pokemon_Types on p.PokemonID equals pt.PokemonId
                                        where p.PokemonID == id
                                        select new PokemonEdit
                                        {
                                            PokemonID = p.PokemonID,
                                            PokemonCode = p.PokemonCode,
                                            PokemonName = p.PokemonName,
                                            Height = p.Height,
                                            Weight = p.Weight,
                                            Gender = p.Gender,
                                            TypeIDs = (from pt in _pokemonDbContext.pokemon_Types
                                                       where pt.PokemonId == id
                                                       select pt.TypeId).ToList()

                                        }).FirstOrDefault();

                    return Json(new { response = _pokemongets, code = 1 });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public IActionResult Edit(PokemonEdit model)
            {
                var employee = _pokemonDbContext.Pokemons.Find(model.PokemonID);
                employee.PokemonName = model.PokemonName;
                employee.PokemonCode = model.PokemonCode;
                employee.Height = model.Height;
                employee.Weight = model.Weight;
                employee.Gender = model.Gender;
                _pokemonDbContext.SaveChanges();
                return Json(new { status = 1, message = "User has been created successfully." });
            }

            public JsonResult Delete(int id)
            {
                try
                {
                    var model = (from p in _pokemonDbContext.Pokemons
                                 where p.PokemonID == id
                                 select p).SingleOrDefault();

                    model.IsDelete = true;
                    if (_pokemonDbContext != null)
                    {
                        _pokemonDbContext.Pokemons.Update(model);
                        _pokemonDbContext.SaveChanges();
                        return Json(new { status = 1, message = model.PokemonName + "has been delete successfully." });
                    }
                    return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }



            private PropertyInfo GetProperty(string name)
            {
                var properties = typeof(PokemonView).GetProperties();
                PropertyInfo prop = null;
                foreach (var item in properties)
                {
                    if (item.Name.ToLower().Equals(name.ToLower()))
                    {
                        prop = item;
                        break;
                    }
                }
                return prop;
            }

        #region Hàm loại bỏ thông tin giống nhau
        class DistinctItemComparer : IEqualityComparer<PokemonView>
        {
            public bool Equals(PokemonView x, PokemonView y)
            {
                return x.PokemonID == y.PokemonID;
            }

            public int GetHashCode(PokemonView obj)
            {
                return obj.PokemonID.GetHashCode();
            }
        }
        #endregion

        public JsonResult GetTypes()
        {
            var typeList = new List<TypeItem>();
            try
            {
                typeList = (from t in _pokemonDbContext.Types
                            select new TypeItem()
                            {
                                TypeID = t.TypeID,
                                TypeName = t.TypeName
                            }).ToList();
            }
            catch (Exception ex)
            {

            }

            return Json(new { response = typeList, code = 1 });

        }

        //public JsonResult GetTypeIdPoke(int id)
        //{
        //    var typeList = new List<TypeItem>();
        //    try
        //    {
        //        typeList = (from pt in _pokemonDbContext.pokemon_Types
        //                    join t in _pokemonDbContext.Types on pt.TypeId equals t.TypeID
        //                    where pt.PokemonId == id
        //                    select new TypeItem()
        //                    {
        //                        TypeID = t.TypeID,
        //                        TypeName = t.TypeName
        //                    }).ToList();
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return Json(new { response = typeList, code = 1 });

        //}
    }
}