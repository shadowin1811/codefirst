﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PokemonCode.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pokemons",
                columns: table => new
                {
                    PokemonID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PokemonCode = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Height = table.Column<decimal>(nullable: true),
                    Weight = table.Column<decimal>(nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    Avatar = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pokemons", x => x.PokemonID);
                });

            migrationBuilder.CreateTable(
                name: "Types",
                columns: table => new
                {
                    TypeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Types", x => x.TypeID);
                });

            migrationBuilder.CreateTable(
                name: "Pokemon_Types",
                columns: table => new
                {
                    PokemonId = table.Column<int>(nullable: false),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pokemon_Types", x => new { x.PokemonId, x.TypeId });
                    table.ForeignKey(
                        name: "FK_Pokemon_Types_Pokemons_PokemonId",
                        column: x => x.PokemonId,
                        principalTable: "Pokemons",
                        principalColumn: "PokemonID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pokemon_Types_Types_TypeId",
                        column: x => x.TypeId,
                        principalTable: "Types",
                        principalColumn: "TypeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Pokemons",
                columns: new[] { "PokemonID", "Avatar", "Gender", "Height", "IsDelete", "Name", "PokemonCode", "Weight" },
                values: new object[,]
                {
                    { 1, null, 0, 2.04m, false, "Bulbasaur ", "00001", 6.8m },
                    { 2, null, 0, 2m, false, "Charmander  ", "00004", 8.5m }
                });

            migrationBuilder.InsertData(
                table: "Types",
                columns: new[] { "TypeID", "Name" },
                values: new object[,]
                {
                    { 1, "Fire" },
                    { 2, "Grass" },
                    { 3, "Poison" }
                });

            migrationBuilder.InsertData(
                table: "Pokemon_Types",
                columns: new[] { "PokemonId", "TypeId" },
                values: new object[] { 2, 1 });

            migrationBuilder.InsertData(
                table: "Pokemon_Types",
                columns: new[] { "PokemonId", "TypeId" },
                values: new object[] { 1, 2 });

            migrationBuilder.InsertData(
                table: "Pokemon_Types",
                columns: new[] { "PokemonId", "TypeId" },
                values: new object[] { 1, 3 });

            migrationBuilder.CreateIndex(
                name: "IX_Pokemon_Types_TypeId",
                table: "Pokemon_Types",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pokemon_Types");

            migrationBuilder.DropTable(
                name: "Pokemons");

            migrationBuilder.DropTable(
                name: "Types");
        }
    }
}
