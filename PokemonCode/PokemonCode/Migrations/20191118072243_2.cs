﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PokemonCode.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pokemon_Types_Pokemons_PokemonId",
                table: "Pokemon_Types");

            migrationBuilder.DropForeignKey(
                name: "FK_Pokemon_Types_Types_TypeId",
                table: "Pokemon_Types");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pokemon_Types",
                table: "Pokemon_Types");

            migrationBuilder.RenameTable(
                name: "Pokemon_Types",
                newName: "pokemon_Types");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Types",
                newName: "TypeName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Pokemons",
                newName: "PokemonName");

            migrationBuilder.RenameIndex(
                name: "IX_Pokemon_Types_TypeId",
                table: "pokemon_Types",
                newName: "IX_pokemon_Types_TypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_pokemon_Types",
                table: "pokemon_Types",
                columns: new[] { "PokemonId", "TypeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_pokemon_Types_Pokemons_PokemonId",
                table: "pokemon_Types",
                column: "PokemonId",
                principalTable: "Pokemons",
                principalColumn: "PokemonID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_pokemon_Types_Types_TypeId",
                table: "pokemon_Types",
                column: "TypeId",
                principalTable: "Types",
                principalColumn: "TypeID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_pokemon_Types_Pokemons_PokemonId",
                table: "pokemon_Types");

            migrationBuilder.DropForeignKey(
                name: "FK_pokemon_Types_Types_TypeId",
                table: "pokemon_Types");

            migrationBuilder.DropPrimaryKey(
                name: "PK_pokemon_Types",
                table: "pokemon_Types");

            migrationBuilder.RenameTable(
                name: "pokemon_Types",
                newName: "Pokemon_Types");

            migrationBuilder.RenameColumn(
                name: "TypeName",
                table: "Types",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "PokemonName",
                table: "Pokemons",
                newName: "Name");

            migrationBuilder.RenameIndex(
                name: "IX_pokemon_Types_TypeId",
                table: "Pokemon_Types",
                newName: "IX_Pokemon_Types_TypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pokemon_Types",
                table: "Pokemon_Types",
                columns: new[] { "PokemonId", "TypeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Pokemon_Types_Pokemons_PokemonId",
                table: "Pokemon_Types",
                column: "PokemonId",
                principalTable: "Pokemons",
                principalColumn: "PokemonID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pokemon_Types_Types_TypeId",
                table: "Pokemon_Types",
                column: "TypeId",
                principalTable: "Types",
                principalColumn: "TypeID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
