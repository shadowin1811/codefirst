﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.Receipt
{
    public class ReceiptSearch
    {
        public string keyword { get; set; }
    }
}
