﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.Receipt
{
    public class UpdateAppointmentDate
    {
        public int IDReceipt { get; set; }
        public DateTime AppointmentDate { get; set; }
    }
}
