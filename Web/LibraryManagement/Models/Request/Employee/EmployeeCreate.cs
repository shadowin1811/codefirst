﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.Employee
{
    public class EmployeeCreate
    {

        public int ID { get; set; }
        [Required(ErrorMessage = "Mã số nhân viên không được để trống")]
        public string CodeEmployees { get; set; }
        [Required(ErrorMessage = "Họ không được để trống")]
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Tên không được để trống")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Ngày sinh không được để trống")]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage = "Ngày vào làm không được để trống")]
        public DateTime DayToDo { get; set; }
        [Required(ErrorMessage = "Giới tính không được để trống")]
        public int Gender { get; set; }
        [Required(ErrorMessage = "Địa chỉ không được để trống")]
        public string Adress { get; set; }
        [Required(ErrorMessage = "Số điện thoại không được để trống")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Email không được để trống")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
    }
}
