﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.Book
{
    public class BookEdit
    {
        public int ID { get; set; }
        [Display(Name = "Mã sách")]
        [Required(ErrorMessage = "Mã sách")]
        public string BookCode { get; set; }

        [Display(Name = "Tên sách")]
        [Required(ErrorMessage = "Tên sách không đưược bỏ trống")]
        public string BookName { get; set; }

        [Display(Name = "Thể loại")]
        public int IDCategory { get; set; }

        [Display(Name = "NXB")]
        public int IDPublishingCompany { get; set; }

        [Display(Name = "Ngày xuất bản")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Ngày xuất bản không được bỏ trống")]
        public DateTime Publishdate { get; set; }

        [Display(Name = "Tác giả")]
        [Required(ErrorMessage = "Tác giả khống được bỏ trống")]
        public string Author { get; set; }

        [Display(Name = "Số trang")]
        [Required(ErrorMessage = "Số trang không được bỏ trống")]
        public int NumberOfPage { get; set; }

        [Display(Name = "Giới thiệu")]
        [Required(ErrorMessage = "Giới thiệu không được bỏ trống")]
        public string Introduce { get; set; }

        [Display(Name = "Số lượng hiện có")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public int AmountAvailable { get; set; }

        [Display(Name = "Danh mục")]
        public int IDCatalog { get; set; }

        [Display(Name = "Phân loại")]
        public bool Status { get; set; }

        [Display(Name = "Hàng")]
        public int IDRow { get; set; }

        //[Display(Name = "Ảnh")]
        //[Required(ErrorMessage = "Ảnh không được bỏ trống")]
        //public string Avatar { get; set; }
    }
}
