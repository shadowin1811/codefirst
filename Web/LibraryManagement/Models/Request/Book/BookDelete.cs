﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.Book
{
    public class BookDelete
    {
        public int ID { get; set; }
    }
}
