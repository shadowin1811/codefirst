﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.Book
{
    public class BookSearch
    {
        public string keyword { get; set; }
    }
}
