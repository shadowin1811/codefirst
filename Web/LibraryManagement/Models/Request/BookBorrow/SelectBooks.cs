﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.BookBorrow
{
    public class SelectBooks
    {
        public int IDLC { get; set; }
        public string keyword { get; set; }
    }
}
