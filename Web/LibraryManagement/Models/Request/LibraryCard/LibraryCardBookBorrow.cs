﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class LibraryCardBookBorrow
    {
        public int IDBook { get; set; }
        public string BookCode { get; set; }
        public string BookName { get; set; }
        public int NumberBorrow { get; set; }
        public int IDReceipt { get; set; }
        public string CodeReceipt { get; set; }
        [DataType(DataType.Date)]
        public DateTime BorrowDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime AppointmentDate { get; set; }
    }
}
