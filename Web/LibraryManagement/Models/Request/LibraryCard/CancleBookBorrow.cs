﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class CancleBookBorrow
    {
        public int IDBook { get; set; }
        public int IDReceipt { get; set; }
    }
}
