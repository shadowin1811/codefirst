﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class CreateReceipt
    {
        public int ID { get; set; }
        public string CodeReciept { get; set; }
        public int IDLibraryCard { get; set; }
        [DataType(DataType.Date)]
        public DateTime BorrowDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime AppointmentDate { get; set; }
        public int IDEmployee { get; set; }
    }
}
