﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class ViewBorrowBookByIDReceipt
    {
        [Display(Name ="IDBook")]
        public int ID { get; set; }
        public int IDReceipt { get; set; }
        [Display(Name = "Mã Sách")]
        public string BookCode { get; set; }
        [Display(Name = "Tên Sách")]
        public string BookName { get; set; }
        [Display(Name = "Tác Giả")]
        public string Author { get; set; }
        [Display(Name = "Thể Loại")]
        public string Category { get; set; }
        public int NumberBorrow { get; set; }
        [Display(Name = "Mã Phiếu Mượn")]
        public string CodeReceipt { get; set; }
        public int IDLibraryCard { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Ngày Mượn")]
        public DateTime BorrowDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Ngày Hẹn trả")]
        public DateTime AppointmentDate { get; set; }
        [Display(Name = "Tình Trạng")]
        public bool Status { get; set; }
        public string StatusStr => Status ? "Đã Trả" : "Chưa Trả";
        [DataType(DataType.Date)]
        public DateTime? ReturnDay { get; set; }
    }
}
