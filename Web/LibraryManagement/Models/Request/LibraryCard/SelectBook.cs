﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class SelectBook
    {
        public int ID { get; set; }

        [Display(Name = "Tên sách")]
        public string BookName { get; set; }

        [Display(Name = "Thể loại")]
        public string Category { get; set; }

        [Display(Name = "Tác giả")]
        public string Author { get; set; }

        [Display(Name = "Phân loại")]
        public string Classify { get; set; }

        [Display(Name = "Ảnh")]
        public string Avatar { get; set; }
    }
}
