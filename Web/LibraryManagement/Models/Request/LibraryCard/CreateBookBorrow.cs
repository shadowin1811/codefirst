﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class CreateBookBorrow
    {
        public int IDReciept { get; set; }
        public int IDBook { get; set; }
    }
}
