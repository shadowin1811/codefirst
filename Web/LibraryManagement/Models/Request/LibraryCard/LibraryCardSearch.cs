﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Request.LibraryCard
{
    public class LibraryCardSearch
    {
        public string keyword { get; set; }
    }
}
