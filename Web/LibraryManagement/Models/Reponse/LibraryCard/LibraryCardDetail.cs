﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.LibraryCard
{
    public class LibraryCardDetail
    {
        public int ID { get; set; }
        public string CodeStudent { get; set; }
        public int IDStudent { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Class { get; set; }
        public bool Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int NumberOfViolate { get; set; }
        public int Status { get; set; }
        public string StatusStr => Status == 0 ? "Đang Mở" : (Status == 1 ? "Đang Khóa" : "Khóa Vĩnh Viễn");
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }
    }
}
