﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.LibraryCard
{
    public class LibraryCardView
    {
        [Display(Name ="ID Thẻ Thư Viện")]
        public int ID { get; set; }
        [Display(Name ="Mã Sinh Viên")]
        public string CodeStudent { get; set; }
        public int IDStudent { get; set; }
        [Display(Name = "Họ")]
        public string Firstname { get; set; }
        [Display(Name = "Tên")]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Display(Name = "Số Lần Vi Phạm")]
        public int NumberOfViolate { get; set; }
        [Display(Name = "Tình Trạng")]
        public int Status { get; set; }
        public string StatusStr => Status == 0 ? "Đang Mở" : (Status == 1 ? "Đang Khóa" : "Khóa Vĩnh Viễn");
        [Display(Name = "Ngày Tạo")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Ngày Hết Hạn")]
        public DateTime ExpirationDate { get; set; }
    }
}
