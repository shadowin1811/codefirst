﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.Book
{
    public class BookDetail
    {
        public int ID { get; set; }

        [Display(Name = "Mã sách")]
        public string BookCode { get; set; }

        [Display(Name = "Tên sách")]
        public string BookName { get; set; }

        [Display(Name = "Thể loại")]
        public string Category { get; set; }

        [Display(Name = "NXB")]
        public string PublishingCompany { get; set; }

        [Display(Name = "Ngày xuất bản")]
        [DataType(DataType.Date)]
        public DateTime Publishdate { get; set; }

        [Display(Name = "Tác giả")]
        public string Author { get; set; }

        [Display(Name = "Số trang")]
        public int NumberOfPage { get; set; }

        [Display(Name = "Giới thiệu")]
        public string Introduce { get; set; }

        [Display(Name = "Số lượng hiện có")]
        public int AmountAvailable { get; set; }

        [Display(Name = "Danh mục")]
        public string CatalogName { get; set; }

        [Display(Name = "Phân loại")]
        public bool Status { get; set; }
        public string phanLoai => Status ? "Mượn/Trả" : "Đọc";

        [Display(Name = "Hàng")]
        public int Row { get; set; }

        //[Display(Name = "Ảnh")]
        //public string Avatar { get; set; }
    }
}
