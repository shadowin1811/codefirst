﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.Book.DropList
{
    public class PublishingCompanyView
    {
        public int ID { get; set; }
        public string PublishingCompany { get; set; }
    }
}
