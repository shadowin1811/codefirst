﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.Book.DropList
{
    public class RowView
    {
        public int ID { get; set; }
        public int Row { get; set; }
    }
}
