﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.Employee
{
    public class EmployeeView
    {
        public int ID { get; set; }
       
        public string CodeEmployees { get; set; }
        
        public string Firstname { get; set; }
      
        public string LastName { get; set; }
     
        public string Fullname => Firstname + ' ' + LastName;
     
        public DateTime DateOfBirth { get; set; }
        public string DOB => DateOfBirth.ToString("MM/dd/yyyy");
      
        public int Gender { get; set; }
        public bool IsDelete { get; set; }
        //public string Adress { get; set; }
        //public string PhoneNumber { get; set; }
        //public string Email { get; set; }
    }
}
