﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagement.Models.Reponse.Receipt
{
    public class ReceiptView
    {
        [Display(Name = "ID Phiếu Mượn")]
        public int ID { get; set; }
        [Display(Name = "Mã Phiếu Mượn")]
        public string CodeReceipt { get; set; }
        public int IDLibraryCard { get; set; }
        [Display(Name = "Mã Sinh Viên")]
        public string CodeStudent { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Ngày Mượn")]
        public DateTime BorrowDate { get; set; }
        public string DateStr => BorrowDate.ToString("dd/MM/yyyy ");
        
        [DataType(DataType.Date)]
        public string DateStrA => AppointmentDate.ToString("dd/MM/yyyy ");
        [DataType(DataType.Date)]
        [Display(Name = "Ngày Hẹn Trả")]
        public DateTime AppointmentDate { get; set; }
        [Display(Name = "ID Nhân Viên")]
        public int IDEmployee { get; set; }
        [Display(Name = "Tình Trạng")]

        public bool Status { get; set; }
        public string StatusStr => Status  ? "Đã Trả" :  "Chưa Trả" ;
    }
}
