﻿var receipt = receipt || {};

receipt.drawDataTable = function () {
    $("#tbReceipt").DataTable({
        "processing": true, // for show progress bar  
        "serverSide": true, // for process server side  
        "filter": true, // this is for disable filter (search box)  
        "orderMulti": false, // for disable multiple column at once  
        "ajax": {
            "url": "/Receipt/GetReceipts",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "id",
                "name": "ID",
                "autoWidth": true,
                "title": "ID Phiếu Mượn",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "codeReceipt",
                "name": "CodeReceipt",
                "autoWidth": true,
                "title": "Mã Phiếu Mượn",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "idLibraryCard",
                "name": "IDLibraryCard",
                "autoWidth": true,
                "title": "ID Thẻ Thư Viện",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "dateStr",
                "name": "DateStr",
                "autoWidth": true,
                "title": "Ngày Mượn",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "dateStrA",
                "name": "DateStrA",
                "autoWidth": true,
                "title": "Ngày Hẹn Trả",
                "orderable": true
            },
            {
                "data": "idEmployee",
                "name": "IDEmployee",
                "autoWidth": true,
                "title": "ID nhân Viên",
                "orderable": true
            },
            {
                "data": "statusStr",
                "name": "StatusStr",
                "autoWidth": true,
                "title": "Tình Trạng",
                "orderable": true
            },
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a  href="/LibraryCard/ViewBorrowBookNotReturn/' + data + '"><i style="font-size: 24px" class="fa fa-book"></i> </a>' + ' '                        
                },
                "title": "Actions",
                "orderable": false
            }
        ]

    });
};

receipt.init = function () {
    receipt.drawDataTable();
};

$(document).ready(function () {
    receipt.init();
});

