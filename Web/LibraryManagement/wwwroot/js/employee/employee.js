﻿var employee = employee || {};

employee.drawDataTable = function () {
    $.ajax({
        url: '/employee/GetEmployee',
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $('#tbEmployee').empty();
                $.each(response, function (index, value) {
                    $('#tbEmployee').append("<tr>" +

                        "<td>" + value.codeEmployees + "</td>" +
                        "<td>" + value.fullname + "</td>" +
                        //"<td>" + value.lastName + "</td>" +
                        "<td>" + value.dob + "</td>" +
                        "<td>" + (value.gender ? "Nam" : "Nữ") + "</td>" +
                        "<td>" +
                        "<a href ='javascript:void(0);' onclick='employee.showEditModal(" + value.id + ")' > <i class='fas fa-edit'></i></a>" +
                        "<a href ='javascript:void(0);' onclick='employee.delete(" + value.id + ")'> <i class='fas fa-trash-alt'></i></a>" +
                        "</td > " +
                        "</tr>");
                });
            }
        }
    });

}
$(function () {
    $("#DateOfBirth").datepicker({
        format: 'mm/dd/yyyy'
    })
    $("#DayToDo").datepicker({
        format: 'mm/dd/yyyy'
    })
});

employee.showModal = function () {

    $('#addEditModal').modal('show');
};


employee.save = function () {
    var result = $('#addEditForm').validate();
    if (result) {
        var employeeObj = {};
        employeeObj["ID"] = $('#ID').val();
        employeeObj["CodeEmployees"] = $('#CodeEmployees').val();
        employeeObj["Firstname"] = $('#Firstname').val();
        employeeObj["LastName"] = $('#LastName').val();
        employeeObj["DateOfBirth"] = $('#DateOfBirth').val();
        employeeObj["DayToDo"] = $('#DayToDo').val();
        employeeObj["Gender"] = $('#Gender').val();
        employeeObj["Adress"] = $('#Adress').val();
        employeeObj["PhoneNumber"] = $('#PhoneNumber').val();
        employeeObj["Email"] = $('#Email').val();
        $.ajax({
            url: 'Employee/Save',
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(employeeObj), //convert obj to json
            contentType: 'application/json',
            success: function (data) {
                if (data.status === 1) {
                    alert(data.message);
                    employee.resetForm();
                    $('#addEditModal').modal('hide');
                    employee.drawDataTable();
                }
            }
        });
    }
    
};
employee.delete = function (id) {
    $.ajax({
        url: 'Employee/Delete/' + id,
        method: 'DELETE',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.status === 1) {
                employee.drawDataTable();
            }
        }

    });
};

employee.resetForm = function () {
    $('#ID').val(0);
    $('#CodeEmployees').val('');
    $('#Firstname').val('');
    $('#LastName').val('');
    $('#DateOfBirth').val('');
    $('#DayToDo').val('');
    $('#Gender').val('');
    $('#Adress').val('');
    $('#PhoneNumber').val('');
    $('#Email').val('');
    $('#addEditModal').find('.modal-title').text("Add Employee");
};

employee.init = function () {
    employee.resetForm();
    employee.drawDataTable();
};

employee.showEditModal = function (id) {
    $.ajax({
        url: 'Employee/Get/' + id,
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;

                $('#ID').val(response.id);
                $('#CodeEmployees').val(response.codeEmployees);
                $('#Firstname').val(response.firstname);
                $('#LastName').val(response.lastName);
                $('#DateOfBirth').val(moment(response.dateOfBirth).format('MM/DD/YYYY'));
                $('#DayToDo').val(moment(response.dayToDo).format('MM/DD/YYYY'));
                //$('#DateOfBirth').val(response.dateOfBirth);
                //$('#DayToDo').val(response.dayToDo);
                $('#Gender').val(response.gender);
                //$('#Gender').val(response.gender).change(function () {

                //});
                $('#Adress').val(response.adress);
                $('#PhoneNumber').val(response.phoneNumber);
                $('#Email').val(response.email);

                $('#addEditModal').find('.modal-title').text("Edit Employee");
                $('#addEditModal').modal('show');
            }
        }

    });
};



$(document).ready(function () {
    employee.init();
});