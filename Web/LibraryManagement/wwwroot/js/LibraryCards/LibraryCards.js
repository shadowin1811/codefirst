﻿var librarycards = librarycards || {}
librarycards.drawDataTable = function () {
    $("#tblibrarycard").DataTable({
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/LibraryCard/Gets",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "id",
                "name": "ID",
                "autoWidth": true,
                "title": "ID Thẻ",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "codeStudent",
                "name": "CodeStudent",
                "autoWidth": true,
                "title": "Mã Sinh Viên",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "firstname",
                "name": "Firstname",
                "autoWidth": true,
                "title": "Họ ",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "lastName",
                "name": "LastName",
                "autoWidth": true,
                "title": "Tên",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "statusStr",
                "name": "StatusStr",
                "autoWidth": true,
                "title": "Tình Trạng Thẻ",
                "orderable": true
            },
            {
                "data": "id",
                "idlc": "IDLC",
                "render": function (data, type, full, meta) {
                    return '<a href ="javascript: void (0);" onclick="librarycards.showEditModal(' + data + ')" ><i style="font-size: 24px" class="fa fa-user"></i></a>  ' + ' &nbsp&nbsp '
                        + '<a href="/LibraryCard/CreateReceipt/' + data + '"><i style="font-size: 24px" class="fa fa-plus-circle"></i>  </a>  ' + ' &nbsp&nbsp  '
                        +  '<a  href="/LibraryCard/ViewReceipt/' + data + '"><i style="font-size: 24px" class="fa fa-book"></i>  </a>' + '  '
                },
                "title": "Actions",
                "orderable": false
            }
        ]
    });
};
librarycards.showModal = function () {
    $('#addEditLibraryCard').modal('show');
};

librarycards.showEditModal = function (id) {
    $.ajax({
        url: 'LibraryCard/LibraryCardDetail/' + id,
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $('#ID').val(response.id);
                $('#CodeStudent').val(response.codeStudent);
                $('#Firstname').val(response.firstName);
                $('#LastName').val(response.lastName);
                $('#DateOfBirth').val(moment(response.dateOfBirth).format('MM/DD/YYYY'));
                $('#Gender').val(response.gender ? "Nam" : "Nữ");
                $('#Adress').val(response.address);
                $('#PhoneNumber').val(response.phoneNumber);
                $('#Email').val(response.email);


            }
        }
    });
    $('#addEditLibraryCard').find('.modal-title').text("Detail Thẻ Thư Viện");
    $('#addEditLibraryCard').modal('show');
};
librarycards.init = function () {
    librarycards.drawDataTable();
};
$(document).ready(function () {
    librarycards.init();
});

