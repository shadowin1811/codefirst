﻿var book = book || {};


book.drawDataTable = function () {
    $("#tbBook").DataTable({
        "processing": true, // for show progress bar  
        "serverSide": true, // for process server side  
        "filter": true, // this is for disable filter (search box)  
        "orderMulti": false, // for disable multiple column at once  
        "ajax": {
            "url": "/Book/Gets",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            //{
            //    "data": "avatar",
            //    "name": "Avatar",
            //    "autoWidth": true,
            //    "title": "Ảnh",
            //    "render": function (data) {
            //        return '<img src="~/images/' + data + '" style="width:150px;height:180px" />';
            //    },
            //    "searchable": false,
            //    "orderable": false
            //},
            {
                "data": "bookCode",
                "name": "BookCode",
                "autoWidth": true,
                "title": "Mã sách",
                "searchable": true,
                "orderable": false
            },
            {
                "data": "bookName",
                "name": "BookName",
                "autoWidth": true,
                "title": "Tên sách",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "category",
                "name": "Category",
                "autoWidth": true,
                "title": "Thể loại",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "author",
                "name": "Author",
                "autoWidth": true,
                "title": "Tác giả",
                "searchable": true,
                "orderable": true
            },
            {
                "data": "publishingCompany",
                "name": "PublishingCompany",
                "autoWidth": true,
                "title": "NXB",
                "searchable": false,
                "orderable": false
            },
            {
                "data": "introduce",
                "name": "Introduce",
                "autoWidth": true,
                "title": "Giới thiệu",
                "searchable": false,
                "orderable": false
            },
            {
                "data": "phanLoai",
                "name": "Status",
                "autoWidth": true,
                "title": "Phân loại",
                "orderable": false
            },
            {
                "data": "id",
                render: function (data, type, full, meta) {
                    var editBtn = "<a href='javascript:;' onclick=book.showEditModal('" + full.id + "') style='color:black'><i class='fa fa-edit'></i></a>";
                    return $("<div>").append(editBtn).html();
                },
                "title": "Thao tác",
            }
        ]
    });
};

book.showModal = function () {
    $('#addEditBook').modal('show');
};

book.initCatalog = function () {
    $.ajax({
        url: 'Book/GetCatalog',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $.each(response, function (index, value) {
                    $('#Catalog').append(
                        "<option value=" + value.id + ">" + value.catalogName + "</option>"
                    );
                });
            }
        }
    });
};

book.initCategory = function () {
    $.ajax({
        url: 'Book/GetCategory',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $.each(response, function (index, value) {
                    $('#Category').append(
                        "<option value=" + value.id + ">" + value.category + "</option>"
                    );
                });
            }
        }
    });
};

book.initPublishingCompany = function () {
    $.ajax({
        url: 'Book/GetPublishingCompany',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $.each(response, function (index, value) {
                    $('#PublishingCompany').append(
                        "<option value=" + value.id + ">" + value.publishingCompany + "</option>"
                    );
                });
            }
        }
    });
};

book.initRow = function () {
    $.ajax({
        url: 'Book/GetRow',
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $.each(response, function (index, value) {
                    $('#Row').append(
                        "<option value=" + value.id + ">" + value.row + "</option>"
                    );
                });
            }
        }
    });
};

book.save = function () {  
    var bookObj = {};
    bookObj["BookCode"] = $('#BookCode').val();
    bookObj["BookName"] = $('#BookName').val();
    bookObj["IDCategory"] = $('#Category').val();
    bookObj["IDPublishingCompany"] = $('#PublishingCompany').val();
    bookObj["Publishdate"] = $('#Publishdate').val();
    bookObj["Author"] = $('#Author').val();
    bookObj["NumberOfPage"] = $('#NumberOfPage').val();
    bookObj["Introduce"] = $('#Introduce').val();
    bookObj["AmountAvailable"] = $('#AmountAvailable').val();
    bookObj["IDCatalog"] = $('#Catalog').val();
    bookObj["Status"] = $('#Status').val();
    bookObj["IDRow"] = $('#Row').val();
    $.ajax({
        url: 'Book/Save',
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(bookObj), //convert obj to json
        
        success: function (data) {
            if (data.status === 1) {
                book.resetForm();
                $('#addEditBook').modal('hide');
                book.drawDataTable();
            }
        }
    });
};

book.resetForm = function () {
    $('#BookCode').val('');
    $('#BookName').val('');
    $('#Category').prop('selectedIndex', 0);
    $('#PublishingCompany').prop('selectedIndex', 0);
    $('#Publishdate').val('');
    $('#Author').val('');
    $('#NumberOfPage').val('');
    $('#Introduce').val('');
    $('#AmountAvailable').val('');
    $('#Catalog').prop('selectedIndex', 0);
    $('#Status').prop('selectedIndex', 0);
    $('#Row').prop('selectedIndex', 0);
};

book.init = function () {
    book.initCatalog();
    book.initCategory();
    book.initPublishingCompany();
    book.initRow();
    book.resetForm();
    book.drawDataTable();
};

book.showEditModal = function (id) {
    $.ajax({
        url: 'Book/Get/' + id,
        method: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data.code === 1) {
                var response = data.response;
                $('#BookCode').val(response.bookCode);
                $('#BookName').val(response.bookName);
                $('#Category').prop('selectedIndex', response.iDCategory);
                $('#PublishingCompany').prop('selectedIndex', response.iDPublishingCompany);
                $('#Publishdate').val(response.publishdate);
                $('#Author').val(response.author);
                $('#NumberOfPage').val(response.numberOfPage);
                $('#Introduce').val(response.introduce);
                $('#AmountAvailable').val(response.amountAvailable);
                $('#Catalog').prop('selectedIndex', response.iDCatalog);
                $('#Status').prop('selectedIndex', response.status);
                $('#Row').prop('selectedIndex', response.iDRow);

                $('#addEditBook').find('.modal-title').text("Sửa thông tin sách");
                $('#addEditBook').modal('show');
            }
        }
    });
};

$(document).ready(function () {
    book.init();

    $('.custom-file-input').on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).next('.custom-file-label').html(fileName);
    });
});