#pragma checksum "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "55d993aae5c8fbbbb54a790b342926b02a425413"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_LibraryCard_LibraryCardDetail), @"mvc.1.0.view", @"/Views/LibraryCard/LibraryCardDetail.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/LibraryCard/LibraryCardDetail.cshtml", typeof(AspNetCore.Views_LibraryCard_LibraryCardDetail))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\_ViewImports.cshtml"
using LibraryManagement;

#line default
#line hidden
#line 2 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\_ViewImports.cshtml"
using LibraryManagement.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55d993aae5c8fbbbb54a790b342926b02a425413", @"/Views/LibraryCard/LibraryCardDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7a03b079343b697176e720de9fa2d2a7e6e87ff5", @"/Views/_ViewImports.cshtml")]
    public class Views_LibraryCard_LibraryCardDetail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<LibraryManagement.Models.Reponse.LibraryCard.LibraryCardDetail>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(71, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
  
    ViewData["Title"] = "LibraryCardDetail";

#line default
#line hidden
            BeginContext(126, 119, true);
            WriteLiteral("\r\n<h1>Library CardDetail</h1>\r\n\r\n<div>\r\n\r\n    <hr />\r\n    <dl class=\"row\">\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(246, 38, false);
#line 14 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.ID));

#line default
#line hidden
            EndContext();
            BeginContext(284, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(346, 34, false);
#line 17 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.ID));

#line default
#line hidden
            EndContext();
            BeginContext(380, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(441, 47, false);
#line 20 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.CodeStudent));

#line default
#line hidden
            EndContext();
            BeginContext(488, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(550, 43, false);
#line 23 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.CodeStudent));

#line default
#line hidden
            EndContext();
            BeginContext(593, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(654, 45, false);
#line 26 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.IDStudent));

#line default
#line hidden
            EndContext();
            BeginContext(699, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(761, 41, false);
#line 29 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.IDStudent));

#line default
#line hidden
            EndContext();
            BeginContext(802, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(863, 45, false);
#line 32 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(908, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(970, 41, false);
#line 35 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(1011, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1072, 44, false);
#line 38 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(1116, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1178, 40, false);
#line 41 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(1218, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1279, 41, false);
#line 44 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.Class));

#line default
#line hidden
            EndContext();
            BeginContext(1320, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1382, 37, false);
#line 47 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.Class));

#line default
#line hidden
            EndContext();
            BeginContext(1419, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1480, 42, false);
#line 50 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(1522, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1584, 38, false);
#line 53 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(1622, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1683, 47, false);
#line 56 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.DateOfBirth));

#line default
#line hidden
            EndContext();
            BeginContext(1730, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(1792, 43, false);
#line 59 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.DateOfBirth));

#line default
#line hidden
            EndContext();
            BeginContext(1835, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(1896, 43, false);
#line 62 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.Address));

#line default
#line hidden
            EndContext();
            BeginContext(1939, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(2001, 39, false);
#line 65 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.Address));

#line default
#line hidden
            EndContext();
            BeginContext(2040, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(2101, 41, false);
#line 68 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(2142, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(2204, 37, false);
#line 71 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(2241, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(2302, 47, false);
#line 74 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.PhoneNumber));

#line default
#line hidden
            EndContext();
            BeginContext(2349, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(2411, 43, false);
#line 77 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.PhoneNumber));

#line default
#line hidden
            EndContext();
            BeginContext(2454, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(2515, 51, false);
#line 80 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.NumberOfViolate));

#line default
#line hidden
            EndContext();
            BeginContext(2566, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(2628, 47, false);
#line 83 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.NumberOfViolate));

#line default
#line hidden
            EndContext();
            BeginContext(2675, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(2736, 42, false);
#line 86 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.Status));

#line default
#line hidden
            EndContext();
            BeginContext(2778, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(2840, 38, false);
#line 89 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.Status));

#line default
#line hidden
            EndContext();
            BeginContext(2878, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(2939, 45, false);
#line 92 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.StatusStr));

#line default
#line hidden
            EndContext();
            BeginContext(2984, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(3046, 41, false);
#line 95 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.StatusStr));

#line default
#line hidden
            EndContext();
            BeginContext(3087, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(3148, 47, false);
#line 98 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.CreatedDate));

#line default
#line hidden
            EndContext();
            BeginContext(3195, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(3257, 43, false);
#line 101 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.CreatedDate));

#line default
#line hidden
            EndContext();
            BeginContext(3300, 60, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt class=\"col-sm-2\">\r\n            ");
            EndContext();
            BeginContext(3361, 50, false);
#line 104 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.ExpirationDate));

#line default
#line hidden
            EndContext();
            BeginContext(3411, 61, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd class=\"col-sm-10\">\r\n            ");
            EndContext();
            BeginContext(3473, 46, false);
#line 107 "D:\CodeGym\QuanLyThuVien\WEB\LibraryManagement\Views\LibraryCard\LibraryCardDetail.cshtml"
       Write(Html.DisplayFor(model => model.ExpirationDate));

#line default
#line hidden
            EndContext();
            BeginContext(3519, 51, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div>    \r\n    ");
            EndContext();
            BeginContext(3570, 38, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "55d993aae5c8fbbbb54a790b342926b02a42541317914", async() => {
                BeginContext(3592, 12, true);
                WriteLiteral("Back to List");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3608, 10, true);
            WriteLiteral("\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<LibraryManagement.Models.Reponse.LibraryCard.LibraryCardDetail> Html { get; private set; }
    }
}
#pragma warning restore 1591
