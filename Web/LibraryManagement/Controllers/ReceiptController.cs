﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using LibraryManagement.Models.Reponse.Receipt;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LibraryManagement.Controllers
{    
    public class ReceiptController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetReceipts()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count  
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20  
            var length = Request.Form["length"].FirstOrDefault();
            // Sort Column Name  
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            // Sort Column Direction ( asc ,desc)  
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            // Search Value from (Search box)  
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            var receipts = new List<ReceiptView>();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/getreceipts");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {

                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream).Dispose();
                }
                receipts = JsonConvert.DeserializeObject<List<ReceiptView>>(responseData);


                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    var prop = GetProperty(sortColumn);
                    if (sortColumnDirection == "asc")
                    {
                        receipts = receipts.OrderBy(prop.GetValue).ToList();
                    }
                    else
                    {
                        receipts = receipts.OrderByDescending(prop.GetValue).ToList();
                    }
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    receipts = (from e in receipts
                                where e.CodeReceipt.Contains(searchValue)

                                select e).ToList();
                }

                //total number of rows count   
                recordsTotal = receipts.Count();
                //Paging   
                var data = receipts.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }           
        }
        private PropertyInfo GetProperty(string name)
        {
            var properties = typeof(ReceiptView).GetProperties();
            PropertyInfo prop = null;
            foreach (var item in properties)
            {
                if (item.Name.ToLower().Equals(name.ToLower()))
                {
                    prop = item;
                    break;
                }
            }
            return prop;
        }
    }
}