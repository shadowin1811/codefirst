﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using LibraryManagement.Models.Reponse.Book;
using LibraryManagement.Models.Reponse.Book.DropList;
using LibraryManagement.Models.Request.Book;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LibraryManagement.Controllers
{
    public class BookController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Gets(BookSearch model)
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count  
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20  
            var length = Request.Form["length"].FirstOrDefault();
            // Sort Column Name  
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            // Sort Column Direction ( asc ,desc)  
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            // Search Value from (Search box)  
            model.keyword = Request.Form["search[value]"].FirstOrDefault();

            //Paging Size (10,20,50,100)  
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            var books = new List<BookView>();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/book/gets");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var json = JsonConvert.SerializeObject(model);

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                books = JsonConvert.DeserializeObject<List<BookView>>(result);
            }

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                var prop = GetProperty(sortColumn);
                if (sortColumnDirection == "asc")
                {
                    books = books.OrderBy(prop.GetValue).ToList();
                }
                else
                {
                    books = books.OrderByDescending(prop.GetValue).ToList();
                }
            }

            //total number of rows count   
            recordsTotal = books.Count();
            //Paging   
            var data = books.Skip(skip).Take(pageSize).ToList();
            //Returning Json Data  
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        private PropertyInfo GetProperty(string name)
        {
            var properties = typeof(BookView).GetProperties();
            PropertyInfo prop = null;
            foreach (var item in properties)
            {
                if (item.Name.ToLower().Equals(name.ToLower()))
                {
                    prop = item;
                    break;
                }
            }
            return prop;
        }

        [HttpPost]
        public JsonResult Save([FromBody] BookCreate model)
        {
            if (model.ID == 0)
            {
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/book/create");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    return Json(new { status = 1, message = "Book has been created successfully." });
                }
                return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
            }
            else
            {
                var updateResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/book/update");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    updateResult = bool.Parse(result);
                }
                if (updateResult)
                {
                    return Json(new { status = 1, message = "Book has been updated successfully." });
                }
                return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
            }
        }

        public JsonResult Get(int id)
        {
            var bookEdit = new BookEdit();
            var url = "https://localhost:44394/api/book/get/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                bookEdit = JsonConvert.DeserializeObject<BookEdit>(responseData);
            }
            return Json(new { response = bookEdit, code = 1 });
        }

        public JsonResult GetCatalog()
        {
            var catalogs = new List<CatalogView>();
            var url = "https://localhost:44394/api/catalog/gets";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                catalogs = JsonConvert.DeserializeObject<List<CatalogView>>(responseData);
            }
            return Json(new { response = catalogs, code = 1 });
        }

        public JsonResult GetCategory()
        {
            var categories = new List<CategoryView>();
            var url = "https://localhost:44394/api/category/gets";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                categories = JsonConvert.DeserializeObject<List<CategoryView>>(responseData);
            }
            return Json(new { response = categories, code = 1 });
        }

        public JsonResult GetPublishingCompany()
        {
            var publishingCompanies = new List<PublishingCompanyView>();
            var url = "https://localhost:44394/api/publishingcompany/gets";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                publishingCompanies = JsonConvert.DeserializeObject<List<PublishingCompanyView>>(responseData);
            }
            return Json(new { response = publishingCompanies, code = 1 });
        }

        public JsonResult GetRow()
        {
            var rows = new List<RowView>();
            var url = "https://localhost:44394/api/row/gets";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                rows = JsonConvert.DeserializeObject<List<RowView>>(responseData);
            }
            return Json(new { response = rows, code = 1 });
        }
    }
}





//        public IActionResult Details(int id)
//        {
//            var book = new BookDetail();
//            var url = "https://localhost:44366/api/book/get/" + id;
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.Method = "GET";
//            var response = httpWebRequest.GetResponse();
//            {
//                string responseData;
//                Stream responseStream = response.GetResponseStream();
//                try
//                {
//                    StreamReader streamReader = new StreamReader(responseStream);
//                    try
//                    {
//                        responseData = streamReader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        ((IDisposable)streamReader).Dispose();
//                    }
//                }
//                finally
//                {
//                    ((IDisposable)responseStream)?.Dispose();
//                }

//                book = JsonConvert.DeserializeObject<BookDetail>(responseData);
//            }
//            return View(book);
//        }

//        public IActionResult Create()
//        {
//            TempData["Success"] = null;
//            ViewBag.Catalogs = GetCatalogs();
//            ViewBag.Categories = GetCategories();
//            ViewBag.PublishingCompanies = GetPublishingCompanies();
//            ViewBag.Rows = GetRows();
//            return View();
//        }

//        [HttpPost]
//        public IActionResult Create(BookCreate model)
//        {
//            var files = HttpContext.Request.Form.Files;
//            foreach (var image in files)
//            {
//                if (image != null && image.Length > 0)
//                {
//                    var file = image;
//                    var upload = Path.Combine(_dir, "wwwroot\\images");
//                    if (file.Length > 0)
//                    {
//                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
//                        using (var fileStream = new FileStream(Path.Combine(upload, fileName), FileMode.Create))
//                        {
//                            file.CopyTo(fileStream);
//                            model.Avatar = $"{fileName}";
//                        }
//                    }
//                }
//            }
//            var createResult = false;
//            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44366/api/book/create");
//            httpWebRequest.ContentType = "application/json";
//            httpWebRequest.Method = "POST";

//            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
//            {
//                var json = JsonConvert.SerializeObject(model);

//                streamWriter.Write(json);
//            }

//            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
//            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
//            {
//                var result = streamReader.ReadToEnd();
//                createResult = bool.Parse(result);
//            }
//            if (createResult)
//            {
//                TempData["Success"] = "Sách được thêm thành công";
//            }
//            ViewBag.Catalogs = GetCatalogs();
//            ViewBag.Categories = GetCategories();
//            ViewBag.PublishingCompanies = GetPublishingCompanies();
//            ViewBag.Rows = GetRows();
//            ModelState.Clear();
//            return View(new BookCreate() { });
//        }

//        public IActionResult Edit(int id)
//        {
//            var book = new BookEdit();
//            var url = "https://localhost:44366/api/book/get/" + id;
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.Method = "GET";
//            var response = httpWebRequest.GetResponse();
//            {
//                string responseData;
//                Stream responseStream = response.GetResponseStream();
//                try
//                {
//                    StreamReader streamReader = new StreamReader(responseStream);
//                    try
//                    {
//                        responseData = streamReader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        ((IDisposable)streamReader).Dispose();
//                    }
//                }
//                finally
//                {
//                    ((IDisposable)responseStream)?.Dispose();
//                }

//                book = JsonConvert.DeserializeObject<BookEdit>(responseData);
//            }
//            ViewBag.Catalogs = GetCatalogs();
//            ViewBag.Categories = GetCategories();
//            ViewBag.PublishingCompanies = GetPublishingCompanies();
//            ViewBag.Rows = GetRows();
//            return View(book);
//        }

//        [HttpPost]
//        public IActionResult Edit(BookEdit model)
//        {
//            var files = HttpContext.Request.Form.Files;
//            foreach (var image in files)
//            {
//                if (image != null && image.Length > 0)
//                {
//                    var file = image;
//                    var upload = Path.Combine(_dir, "wwwroot\\images");
//                    if (file.Length > 0)
//                    {
//                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
//                        using (var fileStream = new FileStream(Path.Combine(upload, fileName), FileMode.Create))
//                        {
//                            file.CopyTo(fileStream);
//                            model.Avatar = $"{fileName}";
//                        }
//                    }
//                }
//            }
//            var updateResult = false;
//            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44366/api/book/update");
//            httpWebRequest.ContentType = "application/json";
//            httpWebRequest.Method = "PUT";

//            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
//            {
//                var json = JsonConvert.SerializeObject(model);

//                streamWriter.Write(json);
//            }

//            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
//            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
//            {
//                var result = streamReader.ReadToEnd();
//                updateResult = bool.Parse(result);
//            }
//            return RedirectToAction("Index", "Book");
//        }

//        private List<CatalogView> GetCatalogs()
//        {
//            var catalogs = new List<CatalogView>();
//            var url = "https://localhost:44366/api/catalog/gets";
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.Method = "GET";
//            var response = httpWebRequest.GetResponse();
//            {
//                string responseData;
//                Stream responseStream = response.GetResponseStream();
//                try
//                {
//                    StreamReader streamReader = new StreamReader(responseStream);
//                    try
//                    {
//                        responseData = streamReader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        ((IDisposable)streamReader).Dispose();
//                    }
//                }
//                finally
//                {
//                    ((IDisposable)responseStream)?.Dispose();
//                }

//                catalogs = JsonConvert.DeserializeObject<List<CatalogView>>(responseData);
//            }
//            return catalogs;
//        }

//        private List<CategoryView> GetCategories()
//        {
//            var categories = new List<CategoryView>();
//            var url = "https://localhost:44366/api/category/gets";
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.Method = "GET";
//            var response = httpWebRequest.GetResponse();
//            {
//                string responseData;
//                Stream responseStream = response.GetResponseStream();
//                try
//                {
//                    StreamReader streamReader = new StreamReader(responseStream);
//                    try
//                    {
//                        responseData = streamReader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        ((IDisposable)streamReader).Dispose();
//                    }
//                }
//                finally
//                {
//                    ((IDisposable)responseStream)?.Dispose();
//                }

//                categories = JsonConvert.DeserializeObject<List<CategoryView>>(responseData);
//            }
//            return categories;
//        }

//        private List<PublishingCompanyView> GetPublishingCompanies()
//        {
//            var publishingCompanies = new List<PublishingCompanyView>();
//            var url = "https://localhost:44366/api/publishingcompany/gets";
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.Method = "GET";
//            var response = httpWebRequest.GetResponse();
//            {
//                string responseData;
//                Stream responseStream = response.GetResponseStream();
//                try
//                {
//                    StreamReader streamReader = new StreamReader(responseStream);
//                    try
//                    {
//                        responseData = streamReader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        ((IDisposable)streamReader).Dispose();
//                    }
//                }
//                finally
//                {
//                    ((IDisposable)responseStream)?.Dispose();
//                }

//                publishingCompanies = JsonConvert.DeserializeObject<List<PublishingCompanyView>>(responseData);
//            }
//            return publishingCompanies;
//        }

//        private List<RowView> GetRows()
//        {
//            var rows = new List<RowView>();
//            var url = "https://localhost:44366/api/row/gets";
//            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//            httpWebRequest.Method = "GET";
//            var response = httpWebRequest.GetResponse();
//            {
//                string responseData;
//                Stream responseStream = response.GetResponseStream();
//                try
//                {
//                    StreamReader streamReader = new StreamReader(responseStream);
//                    try
//                    {
//                        responseData = streamReader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        ((IDisposable)streamReader).Dispose();
//                    }
//                }
//                finally
//                {
//                    ((IDisposable)responseStream)?.Dispose();
//                }

//                rows = JsonConvert.DeserializeObject<List<RowView>>(responseData);
//            }
//            return rows;
//        }
//    }
//}