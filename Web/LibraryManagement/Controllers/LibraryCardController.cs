﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using LibraryManagement.Models.Reponse.Book;
using LibraryManagement.Models.Reponse.LibraryCard;
using LibraryManagement.Models.Reponse.Receipt;
using LibraryManagement.Models.Request.Book;
using LibraryManagement.Models.Request.BookBorrow;
using LibraryManagement.Models.Request.LibraryCard;
using LibraryManagement.Models.Request.Receipt;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LibraryManagement.Controllers
{
    public class LibraryCardController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private string _dir;

        public LibraryCardController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _dir = _hostingEnvironment.ContentRootPath;
        }

        private static int IDLibraryCardCreate = 0;
        private static int IDLibraryCardReturn = 0;
        private static int IDReciept = 0;

        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Gets()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var librarycards = new List<LibraryCardView>();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/gets");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";

                var response = httpWebRequest.GetResponse();
                {
                    string responseData;
                    Stream responseStream = response.GetResponseStream();
                    try
                    {
                        StreamReader streamReader = new StreamReader(responseStream);
                        try
                        {
                            responseData = streamReader.ReadToEnd();
                        }
                        finally
                        {
                            ((IDisposable)streamReader).Dispose();
                        }
                    }
                    finally
                    {
                        ((IDisposable)responseStream).Dispose();
                    }
                    librarycards = JsonConvert.DeserializeObject<List<LibraryCardView>>(responseData);
                }

                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    var prop = GetProperty(sortColumn);
                    if (sortColumnDirection == "asc")
                    {
                        librarycards = librarycards.OrderBy(prop.GetValue).ToList();
                    }
                    else
                    {
                        librarycards = librarycards.OrderByDescending(prop.GetValue).ToList();
                    }
                }

                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    librarycards = (from l in librarycards
                                    where l.LastName.Contains(searchValue) ||
                                       l.CodeStudent.Contains(searchValue)
                                    select l).ToList();
                }

                //total number of rows count   
                recordsTotal = librarycards.Count();
                //Paging   
                var data = librarycards.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }
        }
        private PropertyInfo GetProperty(string name)
        {
            var properties = typeof(LibraryCardView).GetProperties();
            PropertyInfo prop = null;
            foreach (var item in properties)
            {
                if (item.Name.ToLower().Equals(name.ToLower()))
                {
                    prop = item;
                    break;
                }
            }
            return prop;
        }
        public JsonResult LibraryCardDetail(int id)
        {
            var librarycard = new LibraryCardDetail();
            var url = "https://localhost:44394/api/librarycard/get/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                librarycard = JsonConvert.DeserializeObject<LibraryCardDetail>(responseData);
            }
            return Json(new { response = librarycard, code = 1 });
        }
        public IActionResult CreateReceiptDetail(int IDReceipt)
        {
            var receipt = new ReceiptView();
            var url = "https://localhost:44394/api/librarycard/getreceipt/" + IDReceipt;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                receipt = JsonConvert.DeserializeObject<ReceiptView>(responseData);
            }
            return View(receipt);
        }
        public IActionResult ViewBookBorrowByIDLC(int id)
        {
            IDLibraryCardReturn = id;
            var books = new List<LibraryCardBookBorrow>();
            var url = "https://localhost:44394/api/librarycard/getbr/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                books = JsonConvert.DeserializeObject<List<LibraryCardBookBorrow>>(responseData);
            }
            return View(books);
        }
        public IActionResult CreateReceipt(int id)
        {
            var model = new CreateReceipt();
            model.BorrowDate = DateTime.Now;
            model.AppointmentDate = DateTime.Now.AddDays(7);
            model.IDLibraryCard = id;
            var createResult = 0;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/createreciept");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var json = JsonConvert.SerializeObject(model);

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                createResult = int.Parse(result);
                IDReciept = createResult;
                IDLibraryCardCreate = id;
            }
            if (createResult != 0)
            {
                TempData["Success"] = "Phiếu mượn được thêm thành công";
            }
            ModelState.Clear();
            return RedirectToAction("CreateReceiptDetail", "LibraryCard", new { IDReceipt = createResult });
        }
        public IActionResult ViewBorrowBookByIDReceipt(int id)
        {
            var Receipt = new List<ViewBorrowBookByIDReceipt>();
            var url = "https://localhost:44394/api/librarycard/viewbr/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }
                Receipt = JsonConvert.DeserializeObject<List<ViewBorrowBookByIDReceipt>>(responseData);
            }
            ViewBag.ID = IDLibraryCardCreate;
            ViewBag.IDReceipt = id;
            return View(Receipt);
        }
        public IActionResult ViewBorrowBookFollowReceipt(int id)
        {
            var Receipt = new List<ViewBorrowBookByIDReceipt>();
            var url = "https://localhost:44394/api/librarycard/viewbr/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }
                Receipt = JsonConvert.DeserializeObject<List<ViewBorrowBookByIDReceipt>>(responseData);
            }
            ViewBag.ID = IDLibraryCardReturn;
            return View(Receipt);
        }
        public IActionResult SelectBook(int id, string keyword)
        {
            var model = new SelectBooks();
            model.IDLC = id;
            model.keyword = keyword;
            var books = new List<BookView>();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/selectbook");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var json = JsonConvert.SerializeObject(model);

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                books = JsonConvert.DeserializeObject<List<BookView>>(result);
            }
            ViewBag.IDReceipt = IDReciept;
            return View(books);

        }
        public IActionResult CreateBookBorrow(int id)
        {
            try
            {
                var model = new CreateBookBorrow();
                model.IDReciept = IDReciept;
                model.IDBook = id;
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/createbookborrow");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    TempData["Success"] = "Sách mượn được thêm thành công";
                }

                return RedirectToAction("SelectBook", "LibraryCard", new { id = IDLibraryCardCreate });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public IActionResult ReturnBook(int IDBook, int IDReceipt)
        {
            try
            {
                var model = new ReturnBook();
                model.IDBook = IDBook;
                model.IDReceipt = IDReceipt;
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/returnbook");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    TempData["Success"] = "Sách mượn được thêm thành công";
                }

                return RedirectToAction("ViewBorrowBookFollowReceipt", "LibraryCard", new { id = IDReceipt });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult CancelBookBorrow(int IDBook)
        {

            try
            {
                var model = new CancleBookBorrow();
                model.IDBook = IDBook;
                model.IDReceipt = IDReciept;
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/cancelbookborrow");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    TempData["Success"] = "Xóa Sách Mượn Thành Công";
                }

                return RedirectToAction("ViewBorrowBookByIDReceipt", "LibraryCard", new { id = IDReciept });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult ViewReceipt(int id)
        {
            IDLibraryCardReturn = id;
            var receipts = new List<ReceiptView>();
            var url = "https://localhost:44394/api/librarycard/viewreceiptbyidlc/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }
                receipts = JsonConvert.DeserializeObject<List<ReceiptView>>(responseData);
            }
            ViewBag.IDReceipt = IDReciept;
            return View(receipts);
        }
        public IActionResult UpdateAppointmentDate(DateTime AppointmentDate, int IDReceipt)
        {
            try
            {
                var model = new UpdateAppointmentDate();
                model.AppointmentDate = AppointmentDate;
                model.IDReceipt = IDReceipt;
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/updateappointmentdate");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    TempData["Success"] = "Gia Hạn thành công";
                }

                return RedirectToAction("ViewReceipt", "LibraryCard", new { id = IDLibraryCardReturn });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult DeleteReceipt(int IDReceipt)
        {
            try
            {
                var model = new CancelReceipt();
                model.IDReceipt = IDReceipt;
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/deletereceipt/" + IDReceipt);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    TempData["Success"] = "Đã Xóa thành công";
                }

                return RedirectToAction("ViewReceipt", "LibraryCard", new { id = IDLibraryCardReturn });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult CancelReceipt(int IDReceipt)
        {
            try
            {
                var model = new CancelReceipt();
                model.IDReceipt = IDReceipt;
                var createResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/librarycard/cancelreceipt");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(model);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    createResult = bool.Parse(result);
                }
                if (createResult)
                {
                    TempData["Success"] = "Đã Xóa thành công";
                }

                return RedirectToAction("Index", "LibraryCard");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IActionResult ViewBorrowBookNotReturn(int id)
        {
            var Receipt = new List<ViewBorrowBookByIDReceipt>();
            var url = "https://localhost:44394/api/librarycard/viewbrnotreturn/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }
                Receipt = JsonConvert.DeserializeObject<List<ViewBorrowBookByIDReceipt>>(responseData);
            }
            ViewBag.ID = IDLibraryCardReturn;
            return View(Receipt);
        }
    }



}

