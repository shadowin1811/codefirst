﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LibraryManagement.Models.Reponse.Employee;
using LibraryManagement.Models.Request.Employee;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LibraryManagement.Controllers
{
    public class EmployeeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public IActionResult Index2()
        {
            var employees = new List<EmployeeView>();
            var url = "https://localhost:44394/api/employee/gets";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                employees = JsonConvert.DeserializeObject<List<EmployeeView>>(responseData);
            }
            return View(employees);
        }

        public JsonResult GetEmployee()
        {
            var employees = new List<EmployeeView>();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/employee/gets");
            httpWebRequest.Method = "GET";

            var reponse = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = reponse.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }
                employees = JsonConvert.DeserializeObject<List<EmployeeView>>(responseData);
            }
            return Json(new { response = employees, code = 1 });
        }

        [HttpPost]
        public JsonResult Save([FromBody] EmployeeCreate model)
        {
            if (model.ID == 0)
            {
                try
                {
                    var createResult = false;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/employee/create");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        var json = JsonConvert.SerializeObject(model);

                        streamWriter.Write(json);
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        createResult = bool.Parse(result);
                    }
                    if (createResult)
                    {
                        return Json(new { status = 1, message = "User has been created successfully." });
                    }
                    return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
                }
                catch( Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                try
                {
                   // model.DateOfBirth = DateTime.Parse(model.DateOfBirth.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                    var editResult = false;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/employee/update");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "PUT";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        var json = JsonConvert.SerializeObject(model);

                        streamWriter.Write(json);
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        editResult = bool.Parse(result);
                    }
                    if (editResult)
                    {
                        return Json(new { status = 1, message = "User has been created successfully." });
                    }
                    return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
        }

        public JsonResult Get(int id)
        {
            var employeeEdit = new EmployeeEdit();
            var url = "https://localhost:44394/api/employee/get/" + id;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            var response = httpWebRequest.GetResponse();
            {
                string responseData;
                Stream responseStream = response.GetResponseStream();
                try
                {
                    StreamReader streamReader = new StreamReader(responseStream);
                    try
                    {
                        responseData = streamReader.ReadToEnd();
                    }
                    finally
                    {
                        ((IDisposable)streamReader).Dispose();
                    }
                }
                finally
                {
                    ((IDisposable)responseStream)?.Dispose();
                }

                employeeEdit = JsonConvert.DeserializeObject<EmployeeEdit>(responseData);
            }
            return Json(new { response = employeeEdit, code = 1 });
        }

        public JsonResult Delete(int id)
        {
            try
            {
                var deleteResult = false;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://localhost:44394/api/employee/delete/" + id);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "DELETE";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(id);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    deleteResult = bool.Parse(result);
                }
                if (deleteResult)
                {
                    return Json(new { status = 1, message = "User has been created successfully." });
                }
                return Json(new { status = 0, message = "Something went wrong, please contact administrator." });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}